SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;


CREATE TABLE IF NOT EXISTS `agencies` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `agencies_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `agencies` (`id`, `title`, `user_id`, `created_at`, `updated_at`) VALUES
(7, 'Delikat', 1, '2023-07-01 13:14:47', NULL);

CREATE TABLE IF NOT EXISTS `cards` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `agency_id` int(10) UNSIGNED NOT NULL,
  `office_id` int(10) UNSIGNED NOT NULL,
  `cards_contacts_id` int(11) DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sale_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apartment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` double DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `landmark` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_or_realtor` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `year_built` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floors_house` int(11) DEFAULT NULL,
  `floors_house_end` int(11) DEFAULT NULL,
  `number_of_floors` int(11) DEFAULT NULL,
  `number_of_floors_end` int(11) DEFAULT NULL,
  `floor_location` longtext COLLATE utf8mb4_unicode_ci,
  `site_url` longtext COLLATE utf8mb4_unicode_ci,
  `number_rooms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_building` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `roof` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_area` double DEFAULT NULL,
  `total_area_end` double DEFAULT NULL,
  `living_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kitchen_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ceiling_height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `condition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `heating` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `electricity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `water_pipes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bathroom` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sewage` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internet` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gas` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `security` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `how_plot_fenced` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entrance_door` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `furniture` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `window` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `archive_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `payments` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apartment_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `household_appliances` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `will_live` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `elevator` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gate_width` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `limes` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `garage_height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `garage_length` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `garage_width` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ceiling` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `basement` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `balcony` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `corner` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `view_from_windows` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `garbage_chute` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reason_for_sale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subcategory` longtext COLLATE utf8mb4_unicode_ci,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `creator_id` int(11) DEFAULT NULL,
  `commission` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contract_expiration_date` bigint(20) DEFAULT NULL,
  `is_archived` tinyint(1) NOT NULL,
  `number_contract` bigint(20) DEFAULT NULL,
  `stage_transaction` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `data_change_prices` longtext COLLATE utf8mb4_unicode_ci,
  `complete_percent` double(8,2) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_user_id_foreign` (`user_id`),
  KEY `cards_agency_id_foreign` (`agency_id`),
  KEY `cards_office_id_foreign` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cards` (`id`, `user_id`, `agency_id`, `office_id`, `cards_contacts_id`, `type`, `sale_type`, `city`, `area`, `street`, `building`, `apartment`, `price`, `currency`, `landmark`, `owner_or_realtor`, `year_built`, `floors_house`, `floors_house_end`, `number_of_floors`, `number_of_floors_end`, `floor_location`, `site_url`, `number_rooms`, `type_building`, `roof`, `total_area`, `total_area_end`, `living_area`, `kitchen_area`, `ceiling_height`, `condition`, `heating`, `electricity`, `water_pipes`, `bathroom`, `sewage`, `internet`, `gas`, `security`, `land_area`, `how_plot_fenced`, `entrance_door`, `furniture`, `window`, `gate_height`, `archive_date`, `payments`, `apartment_type`, `household_appliances`, `will_live`, `elevator`, `gate_width`, `limes`, `garage_height`, `garage_length`, `garage_width`, `ceiling`, `basement`, `balcony`, `corner`, `view_from_windows`, `garbage_chute`, `layout`, `reason_for_sale`, `subcategory`, `category`, `description`, `comment`, `creator_id`, `commission`, `contract_expiration_date`, `is_archived`, `number_contract`, `stage_transaction`, `data_change_prices`, `complete_percent`, `created_at`, `updated_at`) VALUES
(1, 1, 7, 5, 1, 'rent', 'object', '1', '1', NULL, NULL, NULL, 2000, 'uah', '24', NULL, NULL, 9, NULL, 7, NULL, 'middle', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'yes', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'one', NULL, NULL, NULL, NULL, NULL, 'one_room', 'apartments', NULL, '[{\"user_id\":17,\"created_at\":1632480343279,\"post\":\"не актуально\"},{\"user_id\":17,\"created_at\":1663229437517,\"post\":\"давно не актуально\"}]', NULL, NULL, NULL, 1, NULL, 'poorly', NULL, NULL, '2019-07-18 05:26:22', '2022-09-15 05:10:35');

CREATE TABLE IF NOT EXISTS `cards_contacts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `decision_makers` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `animals` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kind_of_activity` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `leisure` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `years` int(11) DEFAULT NULL,
  `is_realtor` tinyint(1) DEFAULT NULL,
  `is_partner` tinyint(1) DEFAULT NULL,
  `is_client` tinyint(1) DEFAULT NULL,
  `is_married` tinyint(1) DEFAULT NULL,
  `work_place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `car` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `children` int(11) DEFAULT NULL,
  `is_black_list` tinyint(1) DEFAULT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cards_contacts` (`id`, `name`, `email`, `comment`, `decision_makers`, `animals`, `kind_of_activity`, `leisure`, `years`, `is_realtor`, `is_partner`, `is_client`, `is_married`, `work_place`, `car`, `children`, `is_black_list`, `agency_id`, `created_at`, `updated_at`) VALUES
(1, 'Тестовый сотрудник', 'ivanov.petryk@gmail.com', 'Тестовый сотрудник для проверки', NULL, NULL, NULL, NULL, 0, 1, 0, 0, 0, NULL, NULL, 0, 0, 7, '2023-07-01 13:16:20', '2023-07-01 13:16:34');

CREATE TABLE IF NOT EXISTS `cards_contacts_phones` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cards_contacts_id` int(10) UNSIGNED NOT NULL,
  `agency_id` int(11) DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_contacts_phones_cards_contacts_id_foreign` (`cards_contacts_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cards_contacts_phones` (`id`, `cards_contacts_id`, `agency_id`, `phone`, `created_at`, `updated_at`) VALUES
(1, 1, 57, '+380987653421', '2023-06-30 09:17:07', NULL);

CREATE TABLE IF NOT EXISTS `cards_files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_id` int(10) UNSIGNED NOT NULL,
  `file_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_files_card_id_foreign` (`card_id`),
  KEY `cards_files_file_id_foreign` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cards_files` (`id`, `type`, `card_id`, `file_id`, `created_at`, `updated_at`) VALUES
(1, 'audio/*', 1, 1, '2019-07-18 08:01:55', '2019-07-18 08:01:55');

CREATE TABLE IF NOT EXISTS `cards_request_posts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `card_request_id` int(10) UNSIGNED DEFAULT NULL,
  `card_object_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `initial_card` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cards_request_posts` (`id`, `card_request_id`, `card_object_id`, `user_id`, `initial_card`, `post`, `created_at`, `updated_at`) VALUES
(1, 5045, 3807, 1, 'request', 'Сбросила ссылку покупателю.', '2019-10-29 05:37:40', '2019-10-29 05:37:40');

CREATE TABLE IF NOT EXISTS `cards_request_status` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `card_request_id` int(10) UNSIGNED DEFAULT NULL,
  `card_object_id` int(10) UNSIGNED DEFAULT NULL,
  `user_id` int(10) UNSIGNED DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `show_time` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cards_request_status` (`id`, `card_request_id`, `card_object_id`, `user_id`, `status`, `show_time`, `created_at`, `updated_at`) VALUES
(17, 5045, 3807, 1, 'not_suitable', NULL, '2019-10-31 11:49:34', '2019-10-31 11:49:37');

CREATE TABLE IF NOT EXISTS `cards_share` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `card_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cards` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `cards_share_user_id_foreign` (`user_id`),
  KEY `cards_share_card_id_foreign` (`card_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `card_categories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fields` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `card_categories_value_unique` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `card_categories` (`id`, `value`, `title`, `fields`, `created_at`, `updated_at`) VALUES
(1, 'apartments', 'Квартиры', 'total_area living_area kitchen_area year_built floors_house number_of_floors number_rooms type_building ceiling_height condition heating bathroom internet gas security entrance_door furniture window view_from_windows garbage_chute layout', '2018-12-03 15:46:51', '2018-12-03 15:46:51'),
(2, 'houses_cottages', 'Дома и дачи', 'total_area living_area kitchen_area year_built floors_house number_rooms type_building roof ceiling_height condition heating electricity water_pipes bathroom sewage internet gas security land_area how_plot_fenced entrance_door furniture window view_from_windows garbage_chute layout', '2018-12-03 15:46:51', '2018-12-03 15:46:51'),
(3, 'garages', 'Гаражи', NULL, '2018-12-03 15:46:51', '2018-12-03 15:46:51'),
(4, 'area', 'Участки', NULL, '2018-12-03 15:46:51', '2018-12-03 15:46:51'),
(5, 'commercial', 'Коммерческая', NULL, '2018-12-03 15:46:51', '2018-12-03 15:46:51');

CREATE TABLE IF NOT EXISTS `card_subcategories` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `card_categories_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `currencies` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `currencies_value_unique` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `currencies` (`id`, `value`, `title`, `created_at`, `updated_at`) VALUES
(2, 'eur', 'EUR', NULL, NULL),
(3, 'usd', 'USD', NULL, NULL),
(4, 'uah', 'ГРН', NULL, NULL);

CREATE TABLE IF NOT EXISTS `data_change_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `object` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `data_change_logs_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `data_change_logs` (`id`, `object`, `item_id`, `user_id`, `data`, `created_at`, `updated_at`) VALUES
(49678, 'card', 28047, 1, '[{\"field\":\"landmark\",\"old_value\":null,\"new_value\":\"\\u042e\\u043d\\u043e\\u0441\\u0442\\u044c\"},{\"field\":\"street\",\"old_value\":null,\"new_value\":\"\\u0413\\u0434\\u0430\\u043d\\u0446\\u0435\\u0432\\u0441\\u043a\\u0430\\u044f (10 \\u041b\\u0435\\u0442 \\u041e\\u043a\\u0442\\u044f\\u0431\\u0440\\u044f) \\u0443\\u043b.\"},{\"field\":\"price\",\"old_value\":\"8500\",\"new_value\":\"8500\"},{\"field\":\"number_contract\",\"old_value\":null,\"new_value\":\"1\"},{\"field\":\"building\",\"old_value\":null,\"new_value\":\"1\"},{\"field\":\"apartment\",\"old_value\":null,\"new_value\":\"1\"},{\"field\":\"commission\",\"old_value\":null,\"new_value\":\"2345\"},{\"field\":\"description\",\"old_value\":null,\"new_value\":\"\\u041e\\u041f\\u0418\\u0421\\u0410\\u041d\\u0418\\u0415\"},{\"field\":\"contract_expiration_date\",\"old_value\":null,\"new_value\":\"2023-07-20\"},{\"field\":\"payments\",\"old_value\":null,\"new_value\":\"\\u041a\\u043e\\u043c\\u043c\\u0443\\u043d\\u0430\\u043b\\u044c\\u043d\\u044b\\u0435 \\u0443\\u0441\\u043b\\u0443\\u0433\\u0438\"},{\"field\":\"household_appliances\",\"old_value\":null,\"new_value\":\"\\u042d\\u043b\\u0435\\u043a\\u0442\\u0440\\u043e\\u0447\\u0430\\u0439\\u043d\\u0438\\u043a, \\u041a\\u043e\\u0444\\u0435\\u043c\\u0430\\u0448\\u0438\\u043d\\u0430, \\u0424\\u0435\\u043d, \\u041f\\u043b\\u0438\\u0442\\u0430, \\u0414\\u0443\\u0445\\u043e\\u0432\\u043e\\u0439 \\u0448\\u043a\\u0430\\u0444\"},{\"field\":\"will_live\",\"old_value\":null,\"new_value\":\"\\u0414\\u0435\\u0432\\u0443\\u0448\\u043a\\u0430, \\u041f\\u0430\\u0440\\u0435\\u043d\\u044c, \\u0421\\u0435\\u043c\\u044c\\u044f, \\u0421\\u0442\\u0443\\u0434\\u0435\\u043d\\u0442\\u044b\"},{\"field\":\"elevator\",\"new_value\":\"\\u0415\\u0441\\u0442\\u044c\"},{\"field\":\"type_building\",\"new_value\":\"\\u041a\\u0438\\u0440\\u043f\\u0438\\u0447\\u043d\\u044b\\u0439\"},{\"field\":\"condition\",\"new_value\":\"\\u0415\\u0432\\u0440\\u043e\\u0440\\u0435\\u043c\\u043e\\u043d\\u0442\"},{\"field\":\"heating\",\"old_value\":null,\"new_value\":\"\\u0426\\u0435\\u043d\\u0442\\u0440\\u0430\\u043b\\u044c\\u043d\\u043e\\u0435\"},{\"field\":\"bathroom\",\"new_value\":\"\\u0420\\u0430\\u0437\\u0434\\u0435\\u043b\\u044c\\u043d\\u044b\\u0439\"},{\"field\":\"entrance_door\",\"new_value\":\"\\u041c\\u0435\\u0442\\u0430\\u043b\\u043b\\u0438\\u0447\\u0435\\u0441\\u043a\\u0430\\u044f\"},{\"field\":\"furniture\",\"new_value\":\"\\u0427\\u0430\\u0441\\u0442\\u0438\\u0447\\u043d\\u043e \\u043c\\u0435\\u0431\\u043b\\u0438\\u0440\\u043e\\u0432\\u0430\\u043d\\u0430\"},{\"field\":\"window\",\"new_value\":\"\\u0427\\u0430\\u0441\\u0442\\u0438\\u0447\\u043d\\u043e \\u043c\\u0435\\u0442\\u0430\\u043b\\u043b\\u043e\\u043f\\u043b\\u0430\\u0441\\u0442\\u0438\\u043a\\u043e\\u0432\\u044b\\u0435\"},{\"field\":\"view_from_windows\",\"new_value\":\"\\u0412\\u043e \\u0434\\u0432\\u043e\\u0440 \\u0438 \\u043d\\u0430 \\u0443\\u043b\\u0438\\u0446\\u0443\"},{\"field\":\"layout\",\"new_value\":\"\\u0420\\u0430\\u0437\\u0434\\u0435\\u043b\\u044c\\u043d\\u0430\\u044f\"},{\"field\":\"corner\",\"new_value\":\"\\u041d\\u0435\\u0442\"},{\"field\":\"balcony\",\"new_value\":\"2 \\u0448\\u0442.\"},{\"field\":\"total_area\",\"old_value\":null,\"new_value\":\"80\"},{\"field\":\"floors_house\",\"old_value\":null,\"new_value\":\"4\"},{\"field\":\"number_of_floors\",\"old_value\":null,\"new_value\":\"4\"},{\"field\":\"living_area\",\"old_value\":null,\"new_value\":\"65\"},{\"field\":\"kitchen_area\",\"old_value\":null,\"new_value\":\"15\"},{\"field\":\"ceiling_height\",\"old_value\":null,\"new_value\":\"2.7\"}]', '2023-07-01 10:43:32', '2023-07-01 10:43:32');

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extension` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hash` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `files` (`id`, `user_id`, `name`, `extension`, `hash`, `type`, `size`, `created_at`, `updated_at`) VALUES
(1, 1, 'gift.gif', 'gif', 'HkucVXTuayEkYk8L6zygXCZ0LDwkeancjuI53VeN.gif', 'image/gif', '272314', '2019-07-11 07:59:33', '2019-07-11 07:59:33'),
(21997, 1, 'Apartament-a-mieszkanie-696x390.jpg', 'jpg', 'Jz7UVAbdHmQ44wqCnEvTIuH720W2LpYETpeMXqcl.jpg', 'image/jpeg', '52325', '2023-07-01 10:42:37', '2023-07-01 10:42:37'),
(21998, 1, 'Apartament-a-mieszkanie-696x390.jpg', 'jpg', '8WLrEPfmjmIqZ8LHckRWJpdfL7tiV12yO5rQ1Oxk.jpg', 'image/jpeg', '52325', '2023-07-01 10:43:02', '2023-07-01 10:43:02');

CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(6, '2016_06_01_000004_create_oauth_clients_table', 2),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(8, '2018_12_03_075948_create_files_table', 3),
(9, '2018_12_03_163331_create_roles_table', 4),
(10, '2018_12_03_163906_add_users_field_role_id', 4),
(11, '2018_12_03_170235_create_card_categories_table', 5),
(12, '2018_12_07_171954_create_currencies_table', 6),
(13, '2018_12_09_120726_add_users_field_surname', 7),
(14, '2018_12_09_120804_add_users_field_middle_name', 7),
(16, '2018_12_09_120848_add_users_field_time_zone', 8),
(18, '2018_12_09_130006_create_users_details_table', 9),
(21, '2018_12_09_132914_create_users_phones_table', 10),
(22, '2018_12_09_132957_create_social_networks_table', 10),
(23, '2018_12_09_132957_create_users_social_networks_table', 10),
(24, '2018_12_10_155411_add_users_details_field_profile_image_id', 11),
(25, '2018_12_15_141213_create_agencies_table', 12),
(26, '2018_12_15_141231_create_offices_table', 12),
(28, '2018_12_15_141246_create_cards_table', 12),
(29, '2018_12_15_141254_create_cards_files_table', 12),
(30, '2018_12_15_145505_add_users_field_agency_id', 13),
(31, '2018_12_15_145520_add_users_field_office_id', 13),
(32, '2018_12_15_145537_add_users_field_offices_partition_id', 13),
(33, '2018_12_23_134759_create_cards_contacts_table', 14),
(34, '2018_12_23_135120_add_cards_field_cards_contacts_id', 14),
(35, '2018_12_23_135144_create_cards_contacts_phones_table', 14),
(36, '2018_12_15_141238_create_offices_partitions_table', 15),
(37, '2018_12_26_151342_add_users_details_field_birthday', 16),
(38, '2018_12_29_151749_add_cards_field_stage_transaction', 17),
(40, '2018_12_29_151844_add_cards_field_number_contract', 17),
(41, '2018_12_29_151933_add_cards_field_contract_expiration_date', 17),
(42, '2018_12_29_152953_add_cards_field_is_archived', 18),
(43, '2018_12_29_151815_add_cards_field_commission', 19),
(44, '2019_01_03_094516_add_cards_contacts_column_agency_id', 20),
(45, '2019_01_03_094548_add_cards_contacts_phones_column_agency_id', 20),
(46, '2019_01_08_194312_add_cards_contacts_field_children', 21),
(47, '2019_01_08_194323_add_cards_contacts_field_car', 21),
(48, '2019_01_08_194337_add_cards_contacts_field_work_place', 21),
(49, '2019_01_08_194350_add_cards_contacts_field_is_married', 21),
(50, '2019_01_08_194401_add_cards_contacts_field_is_client', 21),
(51, '2019_01_08_194412_add_cards_contacts_field_is_partner', 21),
(52, '2019_01_08_194423_add_cards_contacts_field_is_realtor', 21),
(53, '2019_01_08_194434_add_cards_contacts_field_years', 21),
(54, '2019_01_08_194444_add_cards_contacts_field_leisure', 21),
(55, '2019_01_08_194455_add_cards_contacts_field_kind_of_activity', 21),
(56, '2019_01_08_194515_add_cards_contacts_field_animals', 21),
(57, '2019_01_08_194525_add_cards_contacts_field_decision_makers', 21),
(58, '2019_01_08_195114_add_cards_contacts_field_is_black_list', 21),
(59, '2019_01_21_174752_create_tasks_table', 22),
(60, '2019_01_26_132953_create_card_subcategories_table', 23),
(61, '2019_01_26_164138_add_card_categories_field_fields', 23),
(62, '2019_01_26_164649_add_cards_field_number_of_floors', 23),
(63, '2019_01_29_192002_change_cards_field_garbage_chute', 24),
(64, '2019_02_05_190847_change_cards_field_comment', 25),
(65, '2019_02_05_190908_change_cards_field_description', 25),
(66, '2019_02_14_162656_add_cards_contacts_field_comment', 26),
(67, '2019_02_23_155442_change_cards_contacts_phones_column_phone', 27),
(68, '2019_02_27_182538_add_cards_field_subcategory', 27),
(69, '2019_02_27_183356_rename_cards_column', 27),
(70, '2019_03_09_123851_add_cards_field_reason_for_sale', 28),
(71, '2019_03_23_152757_add_files_field_user_id', 29),
(72, '2019_03_23_152830_add_files_field_size', 29),
(73, '2019_04_04_125217_change_cards_column_number_rooms', 30),
(74, '2019_04_04_125259_change_cards_column_year_built', 30),
(75, '2019_06_26_133825_add_cards_field_corner', 31),
(76, '2019_06_26_133928_add_cards_field_balcony', 31),
(77, '2019_06_26_133940_add_cards_field_basement', 31),
(78, '2019_06_26_133956_add_cards_field_ceiling', 31),
(79, '2019_06_26_134011_add_cards_field_garage_width', 31),
(80, '2019_06_26_134020_add_cards_field_garage_length', 31),
(81, '2019_06_26_134035_add_cards_field_garage_height', 31),
(82, '2019_06_26_134047_add_cards_field_limes', 31),
(83, '2019_06_26_142034_add_cards_field_gate_width', 32),
(84, '2019_06_26_142049_add_cards_field_gate_height', 32),
(85, '2019_07_10_125534_add_offices_partitions_field_title', 33),
(86, '2019_07_16_082400_add_cards_field_elevator', 34),
(87, '2019_07_16_082516_add_cards_field_will_live', 34),
(88, '2019_07_16_082529_add_cards_field_household_appliances', 34),
(89, '2019_07_18_124450_add_cards_field_apartment_type', 35),
(90, '2019_07_24_160944_add_cards_field_payments', 36),
(91, '2019_07_29_183141_add_cards_field_creator_id', 37),
(92, '2019_07_29_183212_add_cards_field_archive_date', 37),
(93, '2019_08_17_124821_add_cards_field_data_change_prices', 38),
(94, '2019_08_27_155619_create_data_change_logs_table', 39),
(95, '2019_09_17_175909_change_cards_column_price', 40),
(96, '2019_09_18_162009_add_users_field_is_archived', 41),
(97, '2019_10_04_134203_create_cards_request_posts_table', 42),
(98, '2019_10_04_134214_create_cards_request_status_table', 42),
(99, '2019_10_16_154847_add_cards_field_total_area_end', 42),
(100, '2019_10_16_154958_add_cards_field_floors_house_end', 42),
(101, '2019_10_16_155014_add_cards_field_number_of_floors_end', 42),
(102, '2019_10_16_155055_add_cards_field_floor_location', 42),
(103, '2019_10_16_163730_change_cards_column_total_area_end', 43),
(104, '2019_10_17_192917_change_cards_field_total_area_end', 43),
(105, '2019_10_17_193654_change_cards_field_floors_house_end', 43),
(106, '2019_10_17_193706_change_cards_field_number_of_floors_end', 43),
(107, '2019_10_16_163126_change_cards_column_floors_house', 44),
(108, '2019_10_16_163602_change_cards_column_nubmer_of_floors', 44),
(109, '2019_10_16_163728_change_cards_column_total_area', 44),
(110, '2019_10_17_192736_change_cards_field_total_area', 44),
(111, '2019_10_17_193851_change_cards_field_floor_location', 44),
(112, '2019_11_14_184718_add_cards_request_status_field_show_time', 45),
(113, '2019_12_14_134355_add_cards_field_site_url', 46),
(114, '2019_12_22_124207_create_parser_cards_table', 47),
(116, '2019_12_22_124254_create_parser_cards_phones_table', 48),
(117, '2020_01_26_155945_create_cards_share_table', 49),
(118, '2020_03_31_185327_add_cards_field_complete_percent', 50),
(119, '2020_04_02_120837_change_cards_field_site_url', 50);

CREATE TABLE IF NOT EXISTS `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_access_tokens_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('0028cceb82536d4e30d33032145f5168080d8b2f1c4d2d3471fe5ba15d2b351a0a0db0f9f57707dc', 19, 3, NULL, '[\"*\"]', 0, '2020-08-25 04:42:53', '2020-08-25 04:42:53', '2021-08-25 07:42:53'),
('0158abfcf37913fd7818e28b2df6df38b98e93ad8076a990eee64f0038ca0657ba4ab237742f5e5f', 1, 3, NULL, '[\"*\"]', 0, '2023-07-04 12:11:44', '2023-07-04 12:11:44', '2024-07-04 12:11:44'),
('1f19fe888db3a95e9ea7de9392cb05d2d54cb0db5c708f80b5b28bf41c3387c76a50ea177d0f7db4', 11, 3, NULL, '[\"*\"]', 0, '2023-07-04 12:51:05', '2023-07-04 12:51:05', '2024-07-04 12:51:05'),
('2c569cc29f1892de3a4b7e2ae4075a0543f3d088080afcb1437b7ca6983f9cd482e9449a4922cfb2', 11, 3, NULL, '[\"*\"]', 0, '2023-07-04 13:00:23', '2023-07-04 13:00:23', '2024-07-04 13:00:23'),
('35a5d5e461acfab2074d3919bc6f7e80785caca5cb9100d3a736598b1773fd39a8da58f47591d961', 1, 3, NULL, '[\"*\"]', 0, '2023-07-01 10:19:50', '2023-07-01 10:19:50', '2024-07-01 10:19:50'),
('5aa07ae60b24ef17f652d2e33f87a5ba56d31fb3579c98cfbcd93e48f4e61c35ce71342b531b8923', 1, 3, NULL, '[\"*\"]', 0, '2023-07-04 09:25:49', '2023-07-04 09:25:49', '2024-07-04 09:25:49'),
('63630d5ce7ec1db4971b7370bb5d065de13ff2010dd2f8a607c923772e1696338d56dd0bb5ae5cff', 2, 3, NULL, '[\"*\"]', 0, '2023-07-04 10:46:52', '2023-07-04 10:46:52', '2024-07-04 10:46:52'),
('6938fc4d2d0a8a1f48579f251b17ea3690bd12dcd342c12a56aeabb29d6aad8bc33d50f35824293a', 1, 3, NULL, '[\"*\"]', 0, '2023-07-01 13:09:49', '2023-07-01 13:09:49', '2024-07-01 13:09:49'),
('6ad003a0581241a53eea6181c04f6d4666d24809240e44a4054a920fe42d95c66800aee8136cd473', 17, 3, NULL, '[\"*\"]', 0, '2023-07-01 09:57:00', '2023-07-01 09:57:00', '2024-07-01 09:57:00'),
('75d11de9be61b39c03235bf2895d9870802a6adabb921060b89ed9669618fc111a925d9e75cb7f7b', 1, 3, NULL, '[\"*\"]', 0, '2023-07-04 13:00:04', '2023-07-04 13:00:04', '2024-07-04 13:00:04'),
('7ce9b0860bc2aa093b6345bad83dacdda38dbea62162cadde1412d95abba8ef4ba5aacb5a9690073', 3, 3, NULL, '[\"*\"]', 0, '2023-07-04 13:01:35', '2023-07-04 13:01:35', '2024-07-04 13:01:35'),
('8501e037336720c535086515fec8671242d7ee62672eb03ee9d1d29f55265fac602eb4714f1ab00a', 27, 3, NULL, '[\"*\"]', 0, '2023-07-01 10:03:43', '2023-07-01 10:03:43', '2024-07-01 10:03:43'),
('8ca06db0aa3e113cb95cd496323ec995614194f3f5fc28c7dc3ec5915af57d20ec81c19b3cdeb19d', 27, 3, NULL, '[\"*\"]', 0, '2023-07-01 10:00:46', '2023-07-01 10:00:46', '2024-07-01 10:00:46'),
('8f4d556ffd28c4e0249de1b72d64e7db6d55961dfd84c88dd42b7ca11a82f1070a532a0ee247d4d2', 1, 3, NULL, '[\"*\"]', 0, '2023-07-04 10:46:34', '2023-07-04 10:46:34', '2024-07-04 10:46:34'),
('c6a1c0cf30477124b461cfd942c459024d53c09bc41241cd410dd28e479d6d7e8bd79a6267286a34', 27, 3, NULL, '[\"*\"]', 0, '2023-07-01 10:11:26', '2023-07-01 10:11:26', '2024-07-01 10:11:26'),
('e369730a5ccecd5204836aa2dbead52419b83fd989a99cd3fabf8340a4bd8c53f6d389b0bfd7324b', 2, 3, NULL, '[\"*\"]', 0, '2023-07-04 08:36:25', '2023-07-04 08:36:25', '2024-07-04 08:36:25'),
('fc8cc2398968800a5ca16894256132d74ceef70e6b9831072882b2bd0d87bb973e724def82a5e32c', 11, 3, NULL, '[\"*\"]', 0, '2023-07-04 12:29:45', '2023-07-04 12:29:45', '2024-07-04 12:29:45');

CREATE TABLE IF NOT EXISTS `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(11) NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_clients_user_id_index` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'CRM-backend Personal Access Client', 'KbmgP8AXtXtIsRw069va6fVipCEuEgrdZEIM564c', 'http://localhost', 1, 0, 0, '2018-12-02 16:52:09', '2018-12-02 16:52:09'),
(2, NULL, 'CRM-backend Password Grant Client', '7hIltm6Hq3zA98XJ8ExrdXwgzaRwPk7RaqvRvWUs', 'http://localhost', 0, 1, 0, '2018-12-02 16:52:09', '2018-12-02 16:52:09'),
(3, NULL, 'crm-frontend', 'lAMSjiEobNtUyOExcvdz1qzvXLZgcN7dD95JzHWv', 'http://localhost', 0, 1, 0, '2018-12-02 16:56:24', '2018-12-02 16:56:24');

CREATE TABLE IF NOT EXISTS `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `client_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_personal_access_clients_client_id_index` (`client_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2018-12-02 16:52:09', '2018-12-02 16:52:09');

CREATE TABLE IF NOT EXISTS `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `oauth_refresh_tokens` (`id`, `access_token_id`, `revoked`, `expires_at`) VALUES
('001183b5891f3aacf32c7ad1bb985754f3cf0ecffb18ded7b7a5c9269bc05166f863cab219ca6f4f', 'fdd5edcc6fa2fd20bb3965179e3c3db419954848856e7c749c7caf5ae923e23fda0748f4299cf5f4', 0, '2022-02-16 08:36:46'),
('02be4d84a43a5ff881586db8c9f9d661835c88c822ed985a0ada7a2e13be650b73ee7e2d7bb14253', 'fc8cc2398968800a5ca16894256132d74ceef70e6b9831072882b2bd0d87bb973e724def82a5e32c', 0, '2024-07-04 12:29:45'),
('0aff2913816a0f5600c722c77c32499b83f5457220053e12badfd34213305de08eb0ebdf3f00abea', '1f19fe888db3a95e9ea7de9392cb05d2d54cb0db5c708f80b5b28bf41c3387c76a50ea177d0f7db4', 0, '2024-07-04 12:51:05'),
('0bcf7d4fde756b6ef113b5f82475b0e2f1939b8d40a08c37c76ca2e72014b49b7d8ec7e747145f7d', '8f4d556ffd28c4e0249de1b72d64e7db6d55961dfd84c88dd42b7ca11a82f1070a532a0ee247d4d2', 0, '2024-07-04 10:46:34'),
('15f68386a8a8a7b1653544cc83f76843c834aec86de6aeb41e7f39eaedd82f174506c467a497593c', '6ad003a0581241a53eea6181c04f6d4666d24809240e44a4054a920fe42d95c66800aee8136cd473', 0, '2024-07-01 09:57:00'),
('23f5077382683ee6776fd9777ba708e495057d4e6be6db67e43649999c6aa5634a5980908e630dbb', '75d11de9be61b39c03235bf2895d9870802a6adabb921060b89ed9669618fc111a925d9e75cb7f7b', 0, '2024-07-04 13:00:04'),
('279a1e8d745d8cb8f6f0ccd0d8d87a45deffe1c8a02ac77d8a97077b36c61dd7f97460450f64844e', '5aa07ae60b24ef17f652d2e33f87a5ba56d31fb3579c98cfbcd93e48f4e61c35ce71342b531b8923', 0, '2024-07-04 09:25:49'),
('356e5bced482855955680389adf5b7c1e59ade0fbdf5765de0cb58fd14c710232ceb0bdf9c72a9a8', '8ca06db0aa3e113cb95cd496323ec995614194f3f5fc28c7dc3ec5915af57d20ec81c19b3cdeb19d', 0, '2024-07-01 10:00:46'),
('77c21d29e2b180aa08f475e50760fc0141292add0f8b13736b6353affdee5967679f75d2a559e738', 'e369730a5ccecd5204836aa2dbead52419b83fd989a99cd3fabf8340a4bd8c53f6d389b0bfd7324b', 0, '2024-07-04 08:36:25'),
('90980f6e925cdcdc80eed33eb7f8154af41210591943a12866918e81ae7acb85af5d365008bc8ce4', '0158abfcf37913fd7818e28b2df6df38b98e93ad8076a990eee64f0038ca0657ba4ab237742f5e5f', 0, '2024-07-04 12:11:44'),
('ae2b751ff25eeb29e8c2ba5c34b365f6820afe30e7b6c81c95b1031c03d63f17c78e30e5b9e4ccf4', '8501e037336720c535086515fec8671242d7ee62672eb03ee9d1d29f55265fac602eb4714f1ab00a', 0, '2024-07-01 10:03:43'),
('bad1485ab65d370116d153049bcd030eefb312a9f06c826af7bb871403d5d58ca2a2c6e7edb82bfc', '2c569cc29f1892de3a4b7e2ae4075a0543f3d088080afcb1437b7ca6983f9cd482e9449a4922cfb2', 0, '2024-07-04 13:00:23'),
('c0f616f6018053e2ecbcb8bc7e9d185dd35d1b2348081df42c64024dab0f7c1525c587ebe9d506e3', 'c6a1c0cf30477124b461cfd942c459024d53c09bc41241cd410dd28e479d6d7e8bd79a6267286a34', 0, '2024-07-01 10:11:26'),
('daf5b8996990ac7b88ce2ec28aac004a4be82e88f0095cfb06389f694c3b3218b0f85b2c9777fe76', '7ce9b0860bc2aa093b6345bad83dacdda38dbea62162cadde1412d95abba8ef4ba5aacb5a9690073', 0, '2024-07-04 13:01:35'),
('e93a0ab3935af7373aca910e2bf926ee87ffc74b2348d899e187e68cde7143f905206d94d88ac38d', '6938fc4d2d0a8a1f48579f251b17ea3690bd12dcd342c12a56aeabb29d6aad8bc33d50f35824293a', 0, '2024-07-01 13:09:49'),
('ef2aa58e78aea7f54ddd924a2eaf2bd4607373ba249d39eb03f52857d6f5789f5481fa92beb551ec', '63630d5ce7ec1db4971b7370bb5d065de13ff2010dd2f8a607c923772e1696338d56dd0bb5ae5cff', 0, '2024-07-04 10:46:52'),
('f8ee3bf7d7aff43acbb16e5658b0f246058e74e366789fc6ead91e37c14a26344e48f3b3e183c169', '35a5d5e461acfab2074d3919bc6f7e80785caca5cb9100d3a736598b1773fd39a8da58f47591d961', 0, '2024-07-01 10:19:50');

CREATE TABLE IF NOT EXISTS `offices` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `building` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apartment` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `agency_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offices_user_id_foreign` (`user_id`),
  KEY `offices_agency_id_foreign` (`agency_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `offices` (`id`, `title`, `city`, `area`, `street`, `building`, `apartment`, `user_id`, `agency_id`, `created_at`, `updated_at`) VALUES
(5, 'Одесса', '1', NULL, NULL, NULL, NULL, 1, 7, '2023-07-01 13:15:59', NULL);

CREATE TABLE IF NOT EXISTS `offices_partitions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `office_id` int(10) UNSIGNED NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `offices_partitions_user_id_foreign` (`user_id`),
  KEY `offices_partitions_office_id_foreign` (`office_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `offices_partitions` (`id`, `user_id`, `office_id`, `type`, `title`, `created_at`, `updated_at`) VALUES
(3, 1, 5, 'sale', 'Продажа', '2023-07-01 10:20:37', NULL),
(4, 1, 5, 'rent', 'Аренда', '2023-07-01 10:20:37', NULL);

CREATE TABLE IF NOT EXISTS `parser_cards` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `street` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_of_floors` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `floors_house` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_rooms` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_area` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `site_url` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `comment` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `parser_cards_phones` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parser_cards_id` int(10) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `parser_cards_phones_parser_cards_id_foreign` (`parser_cards_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `actions` longtext COLLATE utf8mb4_unicode_ci,
  `fields` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `roles` (`id`, `name`, `title`, `description`, `actions`, `fields`, `created_at`, `updated_at`) VALUES
(1, 'ROLE_ADMIN', 'Админ', 'ROLE_ADMIN', '[{\"cards\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"agencies\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"card_categories\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"card_subcategories\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"currencies\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"offices\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"offices_partitions\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"roles\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"social_networks\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"tasks\":[\"see\",\"edit\",\"delete\",\"add\"]},\n{\"data_change_logs\":[\"see\"]}]', NULL, '2023-07-01 13:20:54', NULL),
(2, 'ROLE_AGENCY_DIRECTOR', 'Директор', 'ROLE_AGENCY_DIRECTOR', '[{\"cards\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"agencies\":[\"see\",\"edit\"]},{\"card_categories\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"card_subcategories\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"currencies\":[\"see\"]},{\"offices\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"offices_partitions\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"roles\":[\"see\"]},{\"social_networks\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"tasks\":[\"see\",\"edit\",\"delete\",\"add\"]}]', NULL, '2023-07-01 13:20:54', NULL),
(3, 'ROLE_OFFICE_DIRECTOR', 'Руководитель офиса', 'ROLE_OFFICE_DIRECTOR', '[{\"cards\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"agencies\":[\"see\"]},{\"card_categories\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"card_subcategories\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"currencies\":[\"see\"]},{\"offices\":[\"see\",\"edit\"]},{\"offices_partitions\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"roles\":[\"see\"]},{\"social_networks\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"tasks\":[\"see\",\"edit\",\"delete\",\"add\"]}]', NULL, '2023-07-01 13:20:54', NULL),
(4, 'ROLE_RIELTOR', 'Риэлтор', 'ROLE_RIELTOR', '[{\"cards\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"agencies\":[\"see\"]},{\"card_categories\":[\"see\"]},{\"card_subcategories\":[\"see\"]},{\"currencies\":[\"see\"]},{\"offices\":[\"see\"]},{\"offices_partitions\":[\"see\"]},{\"roles\":[\"see\"]},{\"social_networks\":[\"see\",\"edit\",\"delete\",\"add\"]},{\"tasks\":[\"see\",\"edit\",\"delete\",\"add\"]}]', NULL, '2023-07-01 13:20:54', NULL);

CREATE TABLE IF NOT EXISTS `social_networks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `social_networks` (`id`, `title`, `created_at`, `updated_at`) VALUES
(1, 'Viber', '2018-12-09 11:48:21', '2018-12-09 11:48:21'),
(2, 'Whatsapp', '2018-12-09 11:48:31', '2018-12-09 11:48:31'),
(3, 'Telegram', '2018-12-09 11:48:36', '2018-12-09 11:48:36');

CREATE TABLE IF NOT EXISTS `tasks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `cards_contacts_id` int(10) UNSIGNED DEFAULT NULL,
  `card_id` int(10) UNSIGNED DEFAULT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `date_time` timestamp NULL DEFAULT NULL,
  `remind` timestamp NULL DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8mb4_unicode_ci,
  `responsibles` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `agency_id` int(11) DEFAULT NULL,
  `office_id` int(11) DEFAULT NULL,
  `offices_partition_id` int(11) DEFAULT NULL,
  `role_id` tinyint(4) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `middle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `time_zone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_archived` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `agency_id`, `office_id`, `offices_partition_id`, `role_id`, `name`, `email`, `middle_name`, `surname`, `time_zone`, `password`, `remember_token`, `is_archived`, `created_at`, `updated_at`) VALUES
(1, 7, 5, 3, 1, 'Admin', 'nimd23@andelikat.com.ua', NULL, NULL, NULL, '$2y$10$H0PDWplYoV1F1jcC1YrjbuKezvFN7uky6a.OBgXKcEtkPMtNVWAEy', NULL, 0, '2023-06-30 13:21:52', '2023-07-01 10:12:45');

CREATE TABLE IF NOT EXISTS `users_details` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `city` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `birthday` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image_id` int(11) DEFAULT NULL,
  `currency` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_details_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users_details` (`id`, `user_id`, `city`, `birthday`, `postal_code`, `profile_image_id`, `currency`, `created_at`, `updated_at`) VALUES
(11, 11, '', NULL, '', NULL, '', '2023-07-04 15:05:37', NULL);

CREATE TABLE IF NOT EXISTS `users_phones` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_phones_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `users_social_networks` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `social_network_id` int(10) UNSIGNED NOT NULL,
  `value` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `users_social_networks_user_id_foreign` (`user_id`),
  KEY `users_social_networks_social_network_id_foreign` (`social_network_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


ALTER TABLE `agencies`
  ADD CONSTRAINT `agencies_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cards`
  ADD CONSTRAINT `cards_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cards_office_id_foreign` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cards_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cards_contacts_phones`
  ADD CONSTRAINT `cards_contacts_phones_cards_contacts_id_foreign` FOREIGN KEY (`cards_contacts_id`) REFERENCES `cards_contacts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cards_files`
  ADD CONSTRAINT `cards_files_card_id_foreign` FOREIGN KEY (`card_id`) REFERENCES `cards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cards_files_file_id_foreign` FOREIGN KEY (`file_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cards_share`
  ADD CONSTRAINT `cards_share_card_id_foreign` FOREIGN KEY (`card_id`) REFERENCES `cards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cards_share_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `data_change_logs`
  ADD CONSTRAINT `data_change_logs_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `offices`
  ADD CONSTRAINT `offices_agency_id_foreign` FOREIGN KEY (`agency_id`) REFERENCES `agencies` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `offices_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `offices_partitions`
  ADD CONSTRAINT `offices_partitions_office_id_foreign` FOREIGN KEY (`office_id`) REFERENCES `offices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `offices_partitions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `parser_cards_phones`
  ADD CONSTRAINT `parser_cards_phones_parser_cards_id_foreign` FOREIGN KEY (`parser_cards_id`) REFERENCES `parser_cards` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users_details`
  ADD CONSTRAINT `users_details_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users_phones`
  ADD CONSTRAINT `users_phones_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `users_social_networks`
  ADD CONSTRAINT `users_social_networks_social_network_id_foreign` FOREIGN KEY (`social_network_id`) REFERENCES `social_networks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `users_social_networks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
